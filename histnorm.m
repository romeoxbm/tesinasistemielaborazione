%histnorm Calcola l'istogramma normalizzato per la stima della funzione
%densità di probabilità.
%
%INPUT:
%data
%bins - Numero dei bin dell'istogramma.
%
%OUTPUT:
%x - Centri dei bin.
%h - Altezze normalizzate nell'istogramma.
%
% Guastella Francesco, matr. n° 0426942
function [ x, h ] = histnorm( data, bins )
    [ h, x ] = hist( data, bins );
    step = abs( x( 2 ) - x( 1 ) );
    area = sum( step * h );
    h = h / area;
end %function histnorm