function X = acp( X )
%acp Effettua la proiezione del dataset X ad uno spazio a dimensioni
%ridotte utilizzando l'analisi delle componenti principali, nota anche come
%trasformata di Karhunen-Lo�ve.
%
% Guastella Francesco, matr. n� 0426942

%Per prima cosa, ottengo la matrice di covarianza di X
sigma = cov( X );

%Poich� la matrice di covarianza appena ottenuta � simmetrica, posso
%individuare n autovettori (teorema spettrale delle matrici), dove n indica
%la dimensione della matrice sigma, la quale � inoltre quadrata.
%Individuo inoltre gli autovalori, prelevandoli successivamente dalla
%matrice diagonale D.
[V, D] = eig( sigma );
d = diag( D );

%Opero un ordinamento discendente degli autovalori in d, poich�
%l'autovalore con il maggior valore corrisponder� alla varianza della
%componente principale 1, e cos� via..
[d, indices] = sort( d, 'descend' );

%Determino m (coefficiente di riduzione) utilizzando il seguente criterio:
%(LaTeX syntax)
% m = \min_{1 \leq j \leq n} \frac{\sum\limits_{i=1}^j \lambda_i}{\sum\limits_{i=1}^n \lambda_i} \geq 0.95
%
%In questo modo, lo spazio di arrivo avr� le m colonne che conservano il
%95% della varianza del dataset X.
m = min( find( ( cumsum( d ) / sum( d ) ) >= 0.95 ) );

%Costruisco la matrice di passaggio M attraverso la selezionando gli m
%autovettori che corrispondono ai primi m autovalori.
%La matrice V degli autovettori � nota anche come "matrice di rotazione". 
M = V( :, indices( 1:m ) );

%Infine, proietto la matrice X nel nuovo spazio, moltiplicandola per la
%matrice di passaggio M appena ottenuta.
X = X * M;
end %function acm

