function [ out ] = bClassify( X, C, p, density )
%classify Classificatore Bayesiano.
%   Implementazione di un classificatore Bayesiano.
%
% Guastella Francesco, matr. n° 0426942

%Estraggo le classi (senza ripetizioni)
Cu = unique( C );

%Determino il numero di classi K
K = length( Cu );

%Creo un array di celle da utilizzare come testing set. In esso saranno
%presenti gli elementi rimasti a seguito della costruzione del training
%set.
tst = cell( 1, K );

%Vettore riga contenente le probabilità a priori per le K classi.
priorProbs = zeros( 1, K );

switch density
    case 'norm'
        %Creo degli array di celle dove memorizzare la media e la
        %matrice di covarianza per ogni classe (calcolate sul training set).
        mu = cell( 1, K );
        sigma = cell( 1, K );

    case 'hist'
        %Creo un array di celle dove memorizzare gli istogrammi per ogni
        %classe K.
        hists = cell( 1, K );
        
    case { 'kernel', 'kernel-epa' }
        %Creo un array di celle dove memorizzare i valori di densità (f) e
        %i punti (100) dove queste vengono calcolate (xi), nell'ordine
        %[xi; f].
        kernels = cell( 1, K );
        
    otherwise
        disp( 'Invalid density value' );
        return
end %switch

%% Fase di apprendimento e costruzione del testing set.
for k = 1:K
    %Trovo tutti gli indici degli elementi appartenenti alla classe Cu_k e
    %li memorizzo nell'array indices.
    indices = find( strcmp( C, Cu{k} ) );
    
    nk = length( indices );
    
    %Calcolo le probabilità a priori
    priorProbs( k ) = nk / length( X );
    
    %Calcolo il numero di elementi da prelevare per la costruzione del
    %training set della class Cu_k a partire dalla percentuale p passata
    %come parametro in input della funzione e dal numero di elementi nk.
    tsSize = round( nk * p );
    
    %Estraggo un sottoinsieme di X avente tsSize righe e lo utilizzo come
    %training set per la classe Cu_k.
    ts = X( indices( 1:tsSize ), : );
    
    %Estraggo un sottoinsieme di X da utilizzare come testing set, nel
    %quale inserisco gli elementi "residui" dalla costruzione del training
    %set, a partire quindi da tsSize + 1.
    tst{k} = X( indices( tsSize + 1:end ), : );
    
    switch density
        case 'norm'
            %Calcolo adesso la media e la matrice di covarianza per la
            %classe Cu_k
            mu{k} = mean( ts );
            sigma{k} = cov( ts );
            
        case 'hist'
            %Calcolo l'istogramma normalizzato.
            [ x, h ] = histnorm( ts, tsSize );
            hists{k} = [ x, h ];
            
        case { 'kernel', 'kernel-epa' }
            smoother = 'normal';
            if strcmp( density, 'kernel-epa' )
                smoother = 'epanechnikov';
            end

            %Calcolo la densità kernel.
            rs = reshape( ts, [ tsSize * size( X, 2 ), 1 ] );
            [ f, xi ] = ksdensity( rs, 'kernel', smoother );
            kernels{k} = [ xi; f ];
    end %switch
end %for k loop

%% Classificazione

%Qui infine memorizzo gli indici delle etichette delle classi alla quali
%il classificatore assegna ogni elemento del testing set. 
r = cell( 1, K );
tempRes = zeros( 1, K );

for k = 1:K
    elemCount = length( tst{k} );
    r{k} = zeros( 1, elemCount );    
    
    %Preleviamo ogni elemento del training set per la classe Cu_k
    for i= 1:elemCount
        %Prendo l'i-esimo elemento di tst{k} e lo assegno a x.
        x = tst{k}( i, : );
        
        apostProbs = zeros( 1, K );
        for j = 1:K
            switch density
                %Determino la probabilità a posteriori P(C_k|x), assumendo
                %la f(x|C_k) (likelihood) una normale multivariata.
                case 'norm'
                    apostProbs( j ) = priorProbs( j ) * mvnpdf( x, mu{j}, sigma{j} );

                %Determino la probabilità a posteriori P(C_k|x),
                %utilizzando lo stimatore di densità istogramma
                %(normalizzato). Qui determino la f(x|C_k) effettuando una
                %interpolazione 1-D lineare utilizzando l'istogramma
                %calcolato nella fase di training del classificatore.
                case 'hist'
                    apostProbs( j ) = interp1( hists{j}( :, 1 ), hists{j}( :, 2 ), x( 1 ), 'linear' );
                    apostProbs( j ) = apostProbs( j ) * priorProbs( j );

                %Determino la probabilità a posteriori P(C_k|x),
                %utilizzando lo stimatore di densità kernel (Gaussiano o
                %Epanechnikov). Qui determino la f(x|C_k) effettuando una
                %interpolazione 1-D lineare utilizzando le densità
                %calcolate nella fase di training del classificatore.
                case { 'kernel', 'kernel-epa' }
                    apostProbs( j ) = priorProbs( j ) * interp1( kernels{j}( 1 , : ), kernels{j}( 2 , : ), x( 1 ), 'linear' );
            end %switch
        end %for j loop

        %Applico la regola del maximum a posteriori probability
        %e cerco quindi il massimo valore contenuto in
        %apostProbs, memorizzando in w il suo indice.
        [ ~, w ] = max( apostProbs );
        r{k}( i ) = w;
    end %for i loop (Per ogni elemento del testing set della classe Cu_k...)
    
    %Infine valuto il riconoscimento ottenuto per la classe k
    tempRes( k ) = length( find( r{k} == k ) ) / elemCount;
end %for k loop

%% Medio infine i risultati e ritorno l'output della funzione bClassify.
out = mean( tempRes );
end %function classify

