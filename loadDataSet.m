function [ names, X, C ] = loadDataSet( fileName, N, d )
%loadDataSet Funzione che, dati in input il nome del file
% da leggere, il numero di elementi presenti nel dataset (N) e il numero 
% di features (d) per osservazione, ritorna:
% * L'array (di celle) "names" contenente i nomi di sequenza per elemento;
% * La matrice "X" di dimensione Nxd che rappresenta il dataset caricato;
% * L'array (di celle) "C" contenente le classi per elemento;
%
% Guastella Francesco, matr. n� 0426942

%init
names = cell( 1, N );
X     = zeros( N, d );
C     = cell( 1, N );

%Apro e leggo il file
fileid = fopen( fileName, 'r' );
for i = 1:N
    names{ i } = fscanf( fileid, '%s', [1 1] );
    X( i, : )  = fscanf( fileid, '%f', [1 d] );
    C{ i }     = fscanf( fileid, '%s', [1 1] );
end

end %function loadDataSet
