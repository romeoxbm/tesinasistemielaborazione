function [ X, C ] = removeClasses( X, C, threshold )
%removeClasses Elimina le classi aventi cardinalit� inferiore o uguale al
%valore di soglia threshold passato come parametro di input alla funzione.
%
% Guastella Francesco, matr. n� 0426942

%Estraggo le classi (senza ripetizioni)
Cu = unique( C );

for k = 1:length( Cu )
    %Trovo tutti gli indici degli elementi appartenenti alla classe Cu_k e
    %li memorizzo nell'array indices.
    indices = find( strcmp( C, Cu{k} ) );

    if( length( indices ) <= threshold )
        X( indices, : ) = [];
        C( indices ) = [];
    end %if statement
end %for k loop

end %function removeClasses

