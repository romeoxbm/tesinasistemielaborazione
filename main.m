%Script principale
% Guastella Francesco, matr. n� 0426942

%Cancello tutte le variabili eventualmente presenti nel workspace.
clear all

%Chiudo tutte le finestre eventualmente aperte.
close all

%Carico il dataset dal file data.txt, che contiene 336 istanze e 7
%attributi (come specificato nel file che riassume le caratteristiche del
%dataset names.txt).
[~, X, C] = loadDataSet( 'data.txt', 336, 7 );

%Elimino dal dataset tutte le classi con cardinalit� inferiore a 5
[X, C] = removeClasses( X, C, 5 );

%Utilizzo l'analisi delle componenti principali (PCA) per effettuare una
%proiezione dei dati contenuti in X in uno spazio a dimensioni ridotte.
X = acp( X );

dens = { 'norm', 'hist', 'kernel', 'kernel-epa';
    'Normale Multivariata', 'Istogramma', 'Kernel (Gaussiano)', 'Kernel (Epanechnikov)' };

%Valuto le prestazioni del classificatore bayesiano, con e senza l'utilizzo
%dello schema One-vs-All, al variare della percentuale di dati prelevati
%per la costruzione del training set e al variare della stima di densit�,
%eseguendo pi� volte la classificazione e memorizzando i risultati nella
%matrice res, nella quale vengono inseriti i risultati della classificazione
%senza l'utilizzo dello schema One-vs-All nella prima riga, mentre nella
%seconda riga sono presenti i risultati della classificazione che ne fa uso.
%La cardinalit� del training set varia dal 30% all'80% dei dati presenti,
%con passo pari al valore di step.
step = 0.0025;
p = 0.3:step:0.8;
pCount = length( p );
dCount = length( dens );
figure( 'name', [ 'Andamento del riconoscimento medio ( step ' num2str( step * 100 ) '% )' ] );
index = 1;
for i = 1 : dCount
    res = zeros( 2, pCount );
    tic;
    fprintf( '* Sto calcolando "%s"...\n', dens{2, i} );
    for j = 1 : pCount
        res( 1, j ) = bClassify( X, C, p( j ), dens{ 1, i } );
        res( 2, j ) = ovaClassify( X, C, p( j ), dens{ 1, i } );
    end %for j loop
    fprintf( '\tTempo di esecuzione per "%s": %.2f secondi;\n', dens{2, i}, toc );
    
    %Rappresento quindi in un grafico l'andamento del riconoscimento del
    %classificatore Bayesiano implementato in bClassify.m e del
    %classificatore Bayesiano binario implementato in ovaClassify.m
    subplot( round( dCount / 2 ) , 2, index );
    plot( p, res( 1, : ), '-r' );
    hold on;
    plot( p, res( 2, : ), '-g' );
    hold off;
    title( dens{2, i} );
    xlabel( 'Cardinalit� Ts (%)' );
    ylabel( 'Riconoscimento medio' );
    legend( 'Default schema', 'One-vs-All schema' );
    index = index + 1;
end %for i loop
disp( '* Done' );