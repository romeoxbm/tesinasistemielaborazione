function [ out ] = ovaClassify( X, C, p, density )
%ovaClassify Classificatore binario Bayesiano One-vs-All
%   Implementazione di un classificatore binario bayesiano facente uso
%   dello schema One-Vs-All.
%
% Guastella Francesco, matr. n� 0426942

%Estraggo le classi (senza ripetizioni)
Cu = unique( C );

%Determino il numero di classi K
K = length( Cu );

%Creo un array di celle da utilizzare come testing set. In esso saranno
%presenti gli elementi rimasti a seguito della costruzione del training
%set.
tst = cell( 1, K );

%Vettore riga contenente le probabilit� a priori per le K classi.
priorProbs = zeros( 1, K );

switch density
    case 'norm'
        %Creo adesso delle matrici di celle dove memorizzare la media e la
        %matrice di covarianza per ogni classe (calcolate sul training set).
        %Per ognuna delle due matrici mu e sigma memorizzer� nella prima
        %riga la media e la covarianza della classe C_k, mentre nella
        %seconda riga !C_k.
        mu = cell( 2, K );
        sigma = cell( 2, K );

    case 'hist'
        %Creo una matrice di celle dove memorizzare gli istogrammi per ogni
        %classe K (riga 1) e per le classi !K (riga 2).
        hists = cell( 2, K );
        
    case { 'kernel', 'kernel-epa' }
        %Creo una matrice di celle dove memorizzare i valori di densit� (f) e
        %i punti (100) dove queste vengono calcolate (xi), nell'ordine
        %[xi; f]. In particolare, nella prima riga la matrice [xi; f] per
        %la classe K, mentre nella seconda, la matrice [xi; f] per la
        %classe !K.
        kernels = cell( 2, K );
        
    otherwise
        disp( 'Invalid density value' );
        return
end %switch

%% Apprendimento e costruzione del testing set
%Per ognuno dei K classificatori binari...
for k = 1:K
    %Trovo tutti gli indici degli elementi appartenenti alla classe Cu_k e
    %li memorizzo nell'array indices.
    indices = find( strcmp( C, Cu{k} ) );
    
    %Trovo tutti gli indici degli elementi che NON appartengono alla classe
    %Cu_k e li memorizzo nell'array othersIndices.
    othersIndices = find( ~strcmp( C, Cu{k} ) );
    
    nk = length( indices );
    
    %Calcolo le probabilit� a priori per la classe Cu_k
    priorProbs( k ) = nk / length( X );
    
    %Calcolo il numero di elementi da prelevare per la costruzione del
    %training set della classe Cu_k a partire dalla percentuale p passata
    %come parametro in input della funzione e dal numero di elementi nk.
    tsSize = round( nk * p );
    
    %Calcolo il numero di elementi da prelevare per la costruzione del
    %training set della classe che � l'unione di tutte le classi diverse da
    %Cu_k, a partire dalla percentuale p passata come parametro in input della
    %funzione e dal numero di elementi length( othersIndices ).
    othersTsSize = round( length( othersIndices ) * p ); 
    
    %Estraggo un sottoinsieme di X avente tsSize righe e lo utilizzo come
    %training set per la classe Cu_k.
    ts = X( indices( 1:tsSize ), : );
    
    %Estraggo un sottoinsieme di X avente othersTsSize righe e lo utilizzo
    %come training set per la classe che � l'unione di tutte le classi
    %diverse da Cu_k.
    othersTs = X( othersIndices( 1:othersTsSize ), : );
    
    %Estraggo un sottoinsieme di X da utilizzare come testing set, nel
    %quale inserisco gli elementi "residui" dalla costruzione del training
    %set, a partire quindi da tsSize + 1.
    tst{k} = X( indices( tsSize + 1:end ), : );
    
    switch density
        case 'norm'
            %Calcolo la media per la classe Cu_k e per la classe !Cu_k
            mu{1, k} = mean( ts );
            mu{2, k} = mean( othersTs );
            
            %Calcolo le matrici di covarianza per la classe Cu_k e per la
            %classe !Cu_k
            sigma{1, k} = cov( ts );
            sigma{2, k} = cov( othersTs );
        
        case 'hist'
            %Calcolo l'istogramma normalizzato per la classe k e per la
            %classe !k.
            [ x, h ] = histnorm( ts, tsSize );
            hists{1, k} = [ x, h ];
            
            [ x, h ] = histnorm( othersTs, othersTsSize );
            hists{2, k} = [ x, h ];
            
        case { 'kernel', 'kernel-epa' }
            smoother = 'normal';
            if strcmp( density, 'kernel-epa' )
                smoother = 'epanechnikov';
            end

            %Calcolo la densit� kernel per la classe k e per la classe !k.
            rs = reshape( ts, [ tsSize * size( X, 2 ), 1 ] );
            [ f, xi ] = ksdensity( rs, 'kernel', smoother );
            kernels{1, k} = [ xi; f ];
            
            rs = reshape( othersTs, [ othersTsSize * size( X, 2 ), 1 ] );
            [ f, xi ] = ksdensity( rs, 'kernel', smoother );
            kernels{2, k} = [ xi; f ];
    end %switch
end %for k loop

%% Classificazione

%Qui infine memorizzo gli indici delle etichette delle classi alla quali
%il classificatore assegna ogni elemento del testing set. In particolare,
%nella cella di indice i, viene memorizzato un array logico che assume
%valore 1 nel caso in cui la probabilit� (a posteriori) che l'elemento x
%appartenga alla classe Cu_k � maggiore della probabilt� che appartenga
%alle altre classi, mentre assume il valore 0 nel caso opposto.
r = cell( 1, K );
tempRes = zeros( 1, K );

for k = 1:K
    elemCount = length( tst{k} );
    r{k} = zeros( 1, elemCount );
    
    for i = 1:elemCount
        %Prendo l'i-esimo elemento di tst{k} e lo assegno a x.
        x = tst{k}( i, : );        
        
        switch density
            %Determinare la probabilit� a posteriori P(C_k|x), assumendo
            %la f(x|C_k) (likelihood) una normale multivariata.
            case 'norm'
                aposK = priorProbs( k ) * mvnpdf( x, mu{1, k}, sigma{1, k} );
                aposOthers = ( 1 - priorProbs( k ) ) * mvnpdf( x, mu{2, k}, sigma{2, k} );
                
            %Determino la probabilit� a posteriori P(C_k|x),
            %utilizzando lo stimatore di densit� istogramma
            %(normalizzato). Qui determino la f(x|C_k) effettuando una
            %interpolazione 1-D lineare utilizzando l'istogramma
            %calcolato nella fase di training del classificatore, sia per
            %la classe k, che per la classe !C_k.
            case 'hist'
                aposK = interp1( hists{1, k}( :, 1 ), hists{1, k}( :, 2 ), x( 1 ), 'linear' );
                aposK = aposK * priorProbs( k );
                
                aposOthers = interp1( hists{2, k}( :, 1 ), hists{2, k}( :, 2 ), x( 1 ), 'linear' );
                aposOthers = aposOthers * ( 1 - priorProbs( k ) );
             
            %Determino la probabilit� a posteriori P(C_k|x),
            %utilizzando lo stimatore di densit� kernel.
            %Qui determino la f(x|C_k) effettuando una
            %interpolazione 1-D lineare utilizzando le densit� calcolate
            %nella fase di training del classificatore, sia per
            %la classe k, che per la classe !C_k.
            case { 'kernel', 'kernel-epa' }
                aposK = priorProbs( k ) * interp1( kernels{1, k}( 1 , : ), kernels{1, k}( 2 , : ), x( 1 ), 'linear' );
                aposOthers = ( 1 - priorProbs( k ) ) * interp1( kernels{2, k}( 1 , : ), kernels{2, k}( 2 , : ), x( 1 ), 'linear' );
        end %switch
        
        r{k}( i ) = aposK > aposOthers;
    end %for i loop
    
    %Infine valuto il riconoscimento ottenuto per la classe k
    tempRes( k ) = length( find( r{k} ) ) / elemCount;
end %for k loop

%% Medio infine i risultati e ritorno l'output della funzione ovaClassify.
out = mean( tempRes );
end %function ovaClassify

